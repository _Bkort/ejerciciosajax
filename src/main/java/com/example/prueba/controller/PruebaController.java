
package com.example.prueba.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * PruebaController
 */
@Controller
@RequestMapping(value = "prueba")
public class PruebaController {

    @GetMapping(value="vista")
    public String getVista() {
        return new String("index");
    }

    @GetMapping(value="vista2")
    public String getVista2() {
        return new String("index2");
    }

    @GetMapping(value="vista3")
    public String getVista3() {
        return new String("index3");
    }
    
    
}